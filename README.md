# ドローンネットワークを用いた災害救助犬支援システム


### Prerequisites

* Unity

### Installing

プログラムのインストールの仕方
* Step 1: ソースコードをダウンロードし, 以下のフォルダに入れる. 
```
~/Assets/Scenes
```
* Step 2: Unityにexample.PNGの通りにオブジェクトを配置する. 
* Step 3: オブジェクトにScriptをそれぞれ追加する. 

## Running the Examples

Unity内の再生ボタンをクリックする. 

### Setting the examples

```
災害救助犬の捜索とドローンの連携について見ることができる. ドローンの交代による遅延なども確認できる.
```

## Author

* Takahashi Naoki

2020_02_20

